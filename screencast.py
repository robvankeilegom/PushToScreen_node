import pygame, pygame.font, pygame.event, pygame.draw, string
from pygame.locals import *
import time
import config as cfg

pygame.init()
pygame.mouse.set_visible(False)
screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)

done = False
oldmessage = ""
timestamp = 1
while not done:
    fontobject=pygame.font.SysFont('Arial', 120)
    file = open("/var/www/html/PushToScreen_node/text.txt", "r")
    message = file.read()
    file.close()

    messageLength = len(message)
    if messageLength != 0 and message != oldmessage:
        timestamp = time.time()
        oldmessage = message
        i = 0

        screen.fill((cfg.background['r'], cfg.background['g'], cfg.background['b']))
        diplayedCharacters = 0
        while diplayedCharacters < len(message):
            endOfString = diplayedCharacters + 27
            if (endOfString >= len(message)):
                endOfString = len(message)
                placeOfSpace = -1
            else:
                placeOfSpace = message.rfind(' ', diplayedCharacters, endOfString)
            if (placeOfSpace == -1):
                textToDisplay = message[diplayedCharacters:endOfString]
                diplayedCharacters = endOfString
            else:
                textToDisplay = message[diplayedCharacters:placeOfSpace]
                diplayedCharacters = placeOfSpace
            diplayedCharacters = diplayedCharacters + 1
            paddingTop = ( 100 + ( 110 * i ) )
            screen.blit(fontobject.render(textToDisplay, 1, (cfg.fontColor['r'], cfg.fontColor['g'], cfg.fontColor['b'])), (100, paddingTop))
            i = i + 1

    elif ( messageLength <= 1 or ( time.time() - timestamp ) > cfg.resetTime ) and timestamp != 0:
        timestamp = 0
        screen.fill((cfg.afkBackground['r'], cfg.afkBackground['g'], cfg.afkBackground['b']))

    pygame.display.flip()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                done = True

    time.sleep(0.1)

pygame.quit()
