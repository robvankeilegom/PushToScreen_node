<?php
if( isset( $_POST["text"] ) && $_POST["text"] != "") {
  try
  {
    $fileName = "text.txt";

    if ( !file_exists($fileName) ) {
      throw new Exception('File not found.');
    }

    $textFile = fopen($fileName, "w");
    if ( !$textFile ) {
      throw new Exception('File open failed.');
    }

    fwrite($textFile, $_POST["text"]);
    fclose($textFile);
    echo "<script>history.go(-1);</script>";
  } catch ( Exception $e ) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
  }
} else {
  echo "Post variable not set";
}

header('Location: ' . $_SERVER['HTTP_REFERER']);
